<?php
header("Content-Type: text/html; charset=utf-8");
require_once("User.php");
session_start();

if($_POST['login'] && $_POST['passwd']) {
    if( User::checkLoginPasswd($_POST['login'], md5($_POST['passwd'])) ) {
        $_SESSION['login'] = $_POST['login'];
        $_SESSION['passwd'] = md5($_POST['passwd']);
    } else {
        User::showLoginForm('Неверный пароль');
    }
}

if($_GET['act'] == 'logout') {
    User::logout();
}

if(!User::checkLogin()) {
    User::showLoginForm();
}
?>