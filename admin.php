<?php
include("protect.php");
if (!(User::checkLogin() && $_SESSION['login'] == 'admin')) {
    User::showLoginForm();
}
session_start();
error_reporting(0);
header("Content-Type: text/html; charset=utf-8");
require_once('LinksTable.php');
require_once('users_table.php');

if ($_POST["new_link"]) {
    Link::add($_POST['new_link'], $_POST["new_link_description"]);
}

if ($_POST["new_user"]) {
    User::add($_POST['new_user'], $_POST['new_user_passwd']);
}
?>

<html>
<head>
    <link href='/style/LinksTable.css' rel='stylesheet'>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://static.tumblr.com/maopbtg/oimmiw86r/jquery.autosize-min.js"></script>
    <script type="text/javascript" src="/js/admin.js"></script>

</head>
<body>
<div class="content">
    <h1>
        Панель администратора
    </h1>
    <?php
    echo LinksTable::getSearchForm();
    if ($_GET['act'] == 'users') {
        echo '<a href="?act=links">[ссылки]</a> <b>[пользователи]</b> <a href="?act=logout">[выход]</a>';
        echo getAdminUsersTable();
    } else {
        echo '<b>[ссылки]</b> <a href="?act=users">[пользователи]</a> <a href="?act=logout">[выход]</a>';
        echo LinksTable::getAdminLinksTable($_GET["search"]);
    }
    ?>
</div>
</body>
</html>
