<?php
require_once('database.php');

class User {

    public static function add ($login, $passwd) {
        $sql_connection = SQLConnection::get();
        $already_exist =  mysql_num_rows(
                                mysql_query("SELECT *
                                                FROM
                                                  `users`
                                                WHERE
                                                  `login` = '" . $login . "'"
                                            ,$sql_connection) );
        if ($already_exist != 0) {
            throw new Exception("Пользователь с таким логином уже существует");
        }
        mysql_query("INSERT INTO `users`(
                        `login` ,
                        `passwd`)
                     VALUES (
                     '" . $login . "' ,
                     '". md5($passwd) ."'
                     );" ,
                    $sql_connection);

    }

    public static function delete($user_login) {
        if ($user_login == "admin") {
            throw new Exception("Нельзя удалить админа");
        }
        $sql_connection = SQLConnection::get();
        $sql_query = "DELETE FROM
                        `users`
                          WHERE
                            `login` = '" . $user_login . "'";
        mysql_query($sql_query, $sql_connection);
    }

    public static function getAll() {
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                        `login`
                      FROM
                        `users`
                          WHERE
                            `login` != 'admin'";
        $sql_result = mysql_query($sql_query, $sql_connection);
        return $sql_result;
    }

    public static function showLoginForm ($message = 'Доступ запрещён') {
        $html_content = '
    <html>
    <head>
        <link href="/style/protect.css" rel="stylesheet">
    </head>
    <body>
        <video autoplay  poster="style/matrix.png" id="bgvid" loop>
            <source src="style/matrix.webm" type="video/webm">
            <source src="style/matrix.mp4" type="video/mp4">
        </video>
        <div class="login-form">
            <form method="post">
                ' . $message .'. Вход: <br><br>
                <input type="text" name="login" placeholder="Логин">
                <input type="password" name="passwd" placeholder="Пароль">
                <br><br>
                <input type="submit" value="Вход">
            </form>
        </div>
    </body>
    </html>';
        die($html_content);
    }

    public static function checkLoginPasswd($login, $passwd) {
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                        `passwd`
                      FROM
                        `users`
                          WHERE
                            `login` = '" . $login . "'";
        $sql_result = mysql_query($sql_query, $sql_connection);
        $user = mysql_fetch_assoc($sql_result);
        if ($passwd == $user['passwd']) {
            return true;
        } else {
            return false;
        }
    }

    public static function checkLogin(){
        if (isset($_SESSION['login']) && isset($_SESSION['passwd'])) {
            if (self::checkLoginPasswd($_SESSION['login'], $_SESSION['passwd'])) {
                return true;
            }
            return false;
        } else {
            return false;
        }
    }

    public static function login($login, $passwd) {
        if (self::checkLoginPasswd($login, md5($passwd))) {
            $_SESSION['login'] = $login;
            $_SESSION['passwd'] = md5($passwd);
            return true;
        } else {
            return false;
        }
    }

    public static function logout() {
        unset($_SESSION['login']);
        unset($_SESSION['passwd']);
        header('Location: /');
    }

    public static function changePasswd($new_passwd) {
        if (self::checkLogin()) {
            $sql_connection = SQLConnection::get();
            $sql_query = "UPDATE
                            `users`
                          SET
                            `passwd` = '".md5($new_passwd)."'
                                WHERE `login` = '".$_SESSION['login']."'";
            mysql_query($sql_query, $sql_connection);
            if (mysql_affected_rows() == 0) {
                return false;
            } else if (self::login($_SESSION['login'], $new_passwd)) {
                return true;
            }
        } else {
            return false;
        }
    }
}

?>
