<?php
header("Content-Type: text/html; charset=utf-8");
include("protect.php");
require_once("LinksTable.php");

if($_POST['new_passwd']) {
    User::changePasswd($_POST['new_passwd']);
}
?>

<html>
    <head>
        <link href='/style/LinksTable.css' rel='stylesheet'>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script type="text/javascript" src="http://static.tumblr.com/maopbtg/oimmiw86r/jquery.autosize-min.js"></script>
        <script type="text/javascript" src="/js/script.js">
        </script>
    </head>
    <body>
    <div class="content">
        <h1>
            Каталог ссылок
        </h1>
        <?php
        echo LinksTable::getSearchForm();
        if ($_GET['act']=='changepass') {
            echo '<a href="?act=links">[ссылки]</a> <b>[смена пароля]</b>  Вы вошли как <b>'.$_SESSION["login"].'</b> <a href="?act=logout">[выход]</a><br><br>';
            echo LinksTable::getChangePassForm();
        } else {
            echo '<b>[ссылки]</b> <a href="?act=changepass">[смена пароля]</a> Вы вошли как <b>'.$_SESSION["login"].'</b> <a href="?act=logout">[выход]</a><br><br>';
            echo LinksTable::getUserLinksTable($_GET['search']);
        }
        ?>
        </div>
    </body>
</html>