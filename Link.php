<?php
require_once("database.php");
class Link{

    public static function update($link_id, $link, $description) {
        $sql_connection = SQLConnection::get();
        $sql_query = "UPDATE
                        `links`
                      SET
                        `link` = '".$link."',
                        `description` = '".$description."'
                          WHERE `id` = '".$link_id."'";
        mysql_query($sql_query, $sql_connection);
    }

    public static function getAll() {
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                        `id`,
                        `link`,
                        `description`,
                        `date_addition`
                      FROM
                        `links`
                          WHERE
                            1
                      ORDER BY `id` DESC";
        $sql_result = mysql_query($sql_query, $sql_connection);
        return $sql_result;
    }

    public static function getSearch($search_string) {
        $search = substr($search_string, 0, 64);
        $search = preg_replace("/[^\w\x7F-\xFF\s]/", "", $search);
        $search = str_replace(" ","|",$search);
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                    `id`,
                    `link`,
                    `description`,
                    `date_addition`
                  FROM
                    `links`
                      WHERE
                        `description`
                          RLIKE
                            '".$search."'
                  COLLATE utf8_general_ci";
        $sql_result = mysql_query($sql_query, $sql_connection);
        return $sql_result;
    }

    public static function moveDown($link_id) {
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                        MIN(id)
                      FROM
                        `links`";
        $sql_result = mysql_query($sql_query, $sql_connection);
        $result = mysql_fetch_assoc($sql_result);
        if ($link_id == $result["MIN(id)"]) {
            return false;
        } else {
            $sql_query = "SELECT
                            `id`
                          FROM
                            `links`
                              WHERE (
                                `id` = (SELECT MAX(id)
                                  FROM
                                    `links`
                                      WHERE
                                        `id` < '" . $link_id . "'))";
            $sql_result = mysql_query($sql_query, $sql_connection);
            $result = mysql_fetch_assoc($sql_result);
            Link::swap($link_id, $result["id"]);
        }
    }

    public static function moveUp($link_id) {
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                        MAX(id)
                      FROM
                        `links`";
        $sql_result = mysql_query($sql_query, $sql_connection);
        $result = mysql_fetch_assoc($sql_result);
        if ($link_id == $result["MAX(id)"]) {
            return false;
        } else {
            $sql_query = "SELECT
                        `id`
                      FROM
                        `links`
                          WHERE (
                            `id` = (SELECT MIN(id)
                              FROM
                                `links`
                                  WHERE
                                    `id` > '" . $link_id . "'))";
            mysql_query($sql_query, $sql_connection);
            $sql_result = mysql_query($sql_query, $sql_connection);
            $result = mysql_fetch_assoc($sql_result);
            Link::swap($link_id, $result["id"]);
        }
    }

    private static function swap($link_id_1, $link_id_2) {
        $sql_connection = SQLConnection::get();
        $sql_query = "UPDATE
                        `links`
                      SET
                        `id` = '-1'
                          WHERE
                            `id` = '".$link_id_1."'";
        mysql_query($sql_query, $sql_connection);
        $sql_query = "UPDATE
                        `links`
                      SET
                        `id` = '".$link_id_1."'
                          WHERE
                            `id` = '".$link_id_2."'";
        mysql_query($sql_query, $sql_connection);
        $sql_query = "UPDATE
                        `links`
                      SET
                        `id` = '".$link_id_2."'
                          WHERE
                            `id` = '-1'";
        mysql_query($sql_query, $sql_connection);
    }


    public static function add($link, $description) {
        $sql_connection = SQLConnection::get();
        $sql_query = "INSERT INTO `links` (
                        `link` ,
                        `description`,
                        `date_addition`)
                      VALUES (
                        '".$link."',
                        '".$description."',
                        NOW());";
        mysql_query($sql_query, $sql_connection);
    }

    public static function delete($link_id) {
        $sql_connection = SQLConnection::get();
        $sql_query = "DELETE FROM
                        `links`
                          WHERE
                            `id` = '".$link_id."'";
        mysql_query($sql_query, $sql_connection);
    }

    public static function getInfoById($link_id) {
        $sql_connection = SQLConnection::get();
        $sql_query = "SELECT
                `link`,
                `description`,
                `date_addition`
              FROM
                `links`
                  WHERE
                    `id` = '".$link_id."'";
        $sql_result = mysql_query($sql_query, $sql_connection);
        $result = mysql_fetch_assoc($sql_result);
        $final_result = Array(
            'id'     => $link_id,
            'link'        => $result['link'],
            'description'     => $result['description'],
            'date_addition'  => $result['date_addition']
        );
        return $final_result;
    }
}

?>