<?php
include("protect.php");
if (!(User::checkLogin() && $_SESSION['login'] == 'admin')) {
    User::showLoginForm();
}
session_start();
header("Content-Type: text/html; charset=utf-8");

if(session_id() != $_POST['sid']) die('Wrong Request');

require_once("User.php");
switch ($_POST['act']) {
    case "delete":
        User::delete($_POST['user']);
        echo "<td colspan='2'>Пользователь ". $_POST['user'] ." удалён</td>";
        break;
}
?>