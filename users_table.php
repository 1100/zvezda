<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
require_once("User.php");

function getAdminUsersTable()
{
    $html_table = '
        <table>
            <div class="add_form">
                <form action="admin.php?act=users" method="POST">
                    <input type="text" name="new_user" value="" placeholder="Логин"/>
                    <br>
                    <input type="text" name="new_user_passwd" value="" placeholder="Пароль"/>
                    <input class="add" type="submit" value=""/>
                </form>
            </div>
            <thead>
                <tr>
                    <th>Логин</th>
                    <th>*</th>
                </tr>
            </thead>
            <tbody id="users_table">';
    $users_inf = User::getAll();
    while ($user = mysql_fetch_assoc($users_inf)) {
        $html_table .= '
                <tr id="user_' . $user["login"] . '">
                    <td>' . $user["login"] . '</td>
                    <td style="width: 100px;"><button onclick="deleteUser(\'' . $user["login"] . '\', \'' . session_id() . '\')">удалить</button></td>
                </tr>';
    }
    $html_table .= '
            </tbody>
        </table>';
    return $html_table;
}

?>