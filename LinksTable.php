<?php
header("Content-Type: text/html; charset=utf-8");
session_start();
require_once('Link.php');


class LinksTable
{

    private static $search = null;

    static function setSearch($search)
    {
        self::$search = $search;
    }

// ============= ADMIN =============== //
    static function getPositionButtons($link)
    {
        $html_content = '';
        if (!self::$search) {
            $html_content .= '
            <td><button class="small_button up" onclick="upLink(' . $link["id"] . ', \'' . session_id() . '\')"></button></td>
            <td><button class="small_button down" onclick="downLink(' . $link["id"] . ', \'' . session_id() . '\')"></button></td>';
        }
        return $html_content;
    }

    static function getAdminLinkRow($link)
    {
        $html_table_row = '
            <td style="width: 220px;"><a href="' . $link["link"] . '" target="_blank">' . $link["link"] . '</a></td>
            <td><textarea disabled>' . $link["description"] . '</textarea></td>
            <td>' . $link["date_addition"] . '</td>
            <td><button class="small_button edit" onclick="editLink(' . $link["id"] . ', \'' . session_id() . '\', \'' . self::$search . '\')"></button></td>
            <td><button class="small_button delete" onclick="deleteLink(' . $link["id"] . ', \'' . $link["link"] . '\', \'' . session_id() . '\')"></button></td>
            <script type="text/javascript">$(\'textarea\').autosize();</script>';
        $html_table_row .= self::getPositionButtons($link);
        return $html_table_row;
    }

    static function getAdminLinkRowEdit($link)
    {
        $html_table_row = '
            <td style="width: 220px;"><input type="text" id="edit_link_' . $link["id"] . '" value="' . $link["link"] . '"/></td>
            <td><textarea id="edit_description_' . $link["id"] . '">' . $link["description"] . '</textarea></td>
            <td>' . $link["date_addition"] . '</td>
            <td><button class="small_button ok" onclick="updateLink(' . $link["id"] . ', \'' . session_id() . '\', \'' . self::$search . '\')"></button></td>
            <td><button class="small_button cancel" onclick="cancelUpdateLink(' . $link["id"] . ', \'' . session_id() . '\', \'' . self::$search . '\')"></button></td>
            <script type="text/javascript">$(\'textarea\').autosize();</script>';
        $html_table_row .= self::getPositionButtons($link);
        return $html_table_row;
    }

    static function getAdminLinkRowDeleted()
    {
        $html_table_row = '
            <td colspan="7">Удалено</td>';
        return $html_table_row;
    }

    static function getAdminLinksTableBody($links_inf)
    {
        $html_table = '';
        while ($link = mysql_fetch_assoc($links_inf)) {
            $html_table .= '
    <tr id="link_' . $link["id"] . '">
        ' . self::getAdminLinkRow($link) . '
    </tr>';
        }
        return $html_table;
    }

    static function getAdminLinksTable($search)
    {
        $html_table = '
<table>
    <div class="add_form">
        <form action="admin.php" method="POST">
            <input type="text" name="new_link" value="" placeholder="Новая ссылка"/>
            <br>
            <input type="text" name="new_link_description" value="" placeholder="Описание"/>
            <input class="add" type="submit" value=""/>
        </form>
    </div>';
        if ($search) {
            $html_table .= '<br>Поиск по запросу <b>"' . $search . '"</b>: ';
            self::$search = $search;
        } else {
            $html_table .= '';
        }
        $html_table .= '
<thead>
    <tr>
        <th style="width: 250px;">Ссылка</th>
        <th>Описание</th>
        <th style="width: 200px;">Дата</th>
        <th style="width: 25px;">*</th>
        <th style="width: 25px;">*</th>';
        if (!self::$search) {
            $html_table .= '
        <th style="width: 25px;">*</th>
        <th style="width: 25px;">*</th>';
        }
        $html_table .= '
    </tr>
    </thead>
    <tbody id="links_table">';
        if ($search) {
            $links_inf = Link::getSearch(self::$search);
        } else {
            $links_inf = Link::getAll();
        }
        $html_table .= self::getAdminLinksTableBody($links_inf);
        $html_table .= '
    </tbody>
</table>';
        return $html_table;
    }


// ============ U S E R ==============//
    static function getUserLinkRow($link)
    {
        $html_table_row = '
            <td style="width: 220px;"><a href="' . $link["link"] . '" target="_blank">' . $link["link"] . '</a></td>
            <td><textarea disabled>' . $link["description"] . '</textarea></td>
            <td>' . $link["date_addition"] . '</td>
            <script type="text/javascript">$(\'textarea\').autosize();</script>';
        return $html_table_row;
    }

    static function getUserLinkTableBody($links_inf)
    {
        $html_table = '';
        while ($link = mysql_fetch_assoc($links_inf)) {
            $html_table .= '
                <tr>
                    ' . self::getUserLinkRow($link) . '
                </tr>';
        }
        return $html_table;
    }

    static function getUserLinksTable($search)
    {
        if ($search) {
            $html_table = 'Поиск по запросу <b>"' . $search . '"</b>: ';
            self::$search = $search;
        } else {
            $html_table = '';
        }
        $html_table .= '
        <table>
            <thead>
                <tr>
                    <th style="width: 250px">Ссылка</th>
                    <th>Описание</th>
                    <th style="width: 160px;">Дата</th>
                </tr>
            </thead>
            <tbody>';
        if ($search) {
            $links_inf = Link::getSearch(self::$search);
        } else {
            $links_inf = Link::getAll();
        }
        $html_table .= self::getUserLinkTableBody($links_inf);

        $html_table .= '
            </tbody>
        </table>';
        return $html_table;
    }

    static function getChangePassForm()
    {
        $html_form = '
        <div class="change_pass">
            <form method="POST" action="?act=changepass" onSubmit="if(!validatePassword()){return false;};">
                <input type="password" name="new_passwd" id="new_passwd" placeholder="Новый пароль"><br>
                <input type="password" name="new_passwd_repeat" id="new_passwd_repeat" placeholder="Повторите новый пароль"><br><br>
                <input type="submit" value="Сменить пароль">
            </form>
            <div id="err_mes"></div>
        </div>
    ';
        return $html_form;
    }

    static function getSearchForm()
    {
        $html_content = '
        <div class="searchbox">
            <form method="get">
                <input type="text" placeholder="поиск" name="search" class="search_input">
                <input type="submit" class="small_button search" value="">
            </form>
        </div>';
        return $html_content;
    }
}