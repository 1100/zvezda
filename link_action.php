<?php
include("protect.php");
if (!(User::checkLogin() && $_SESSION['login'] == 'admin')) {
    User::showLoginForm();
}
session_start();
header("Content-Type: text/html; charset=utf-8");

require_once('LinksTable.php');

if(session_id() != $_POST['sid']) die('Wrong Request');

if($_POST['search']) LinksTable::setSearch($_POST['search']);

require_once("Link.php");
switch ($_POST['act']) {
    case "delete":
        Link::delete($_POST['id']);
        echo LinksTable::getAdminLinkRowDeleted();
        break;
    case "edit":
        $link = Link::getInfoById($_POST['id']);
        echo LinksTable::getAdminLinkRowEdit($link);
        break;
    case "update":
        Link::update($_POST['id'], $_POST['link'], $_POST['description']);
        $link = Link::getInfoById($_POST['id']);
        echo LinksTable::getAdminLinkRow($link);
        break;
    case "cancel":
        $link = Link::getInfoById($_POST['id']);
        echo LinksTable::getAdminLinkRow($link);
        break;
    case "move_up":
        if ($_GET["search"]) {
            break;
        } else {
            Link::moveUp($_POST["id"]);
            $link = Link::getAll();
            echo LinksTable::getAdminLinksTableBody($link);
        }
        break;
    case "move_down":
        if ($_GET["search"]) {
            break;
        } else {
            Link::moveDown($_POST["id"]);
            $link = Link::getAll();
            echo LinksTable::getAdminLinksTableBody($link);
        }
        break;
}
?>
