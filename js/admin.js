function sendUserRequest(post_request, response_id) {
    $.ajax({
        type: "POST",
        url: "user_action.php",
        data: post_request,
        success: function(response){
            $(response_id).html(response);
        }
    })
}

function sendRequest(post_request, response_id) {
    $.ajax({
        type: "POST",
        url: "link_action.php",
        data: post_request,
        success: function(response){
            $(response_id).html(response);
        }
    })
}

function deleteUser(login, session_id) {
    if (confirm("Удалить пользователя "+login+"?")) {
        var post_request = "sid="+session_id+"&act=delete&user=" + login;
        var response_id = '#user_' + login;
        sendUserRequest(post_request, response_id);
    }
}


function upLink(link_id, session_id) {
    var post_request = "sid="+session_id+"&act=move_up&id="+link_id;
    var response_id = '#links_table';
    sendRequest(post_request, response_id);
}

function downLink(link_id, session_id) {
    var post_request = "sid="+session_id+"&act=move_down&id="+link_id;
    var response_id = '#links_table';
    sendRequest(post_request, response_id);
}

function editLink(link_id, session_id, search) {
    var post_request = "sid="+session_id+"&act=edit&id="+link_id;
    if (search != '') {
        post_request += "&search="+search
    }
    var response_id = '#link_'+link_id;
    sendRequest(post_request, response_id);
}

function updateLink(link_id, session_id, search) {
    var link = document.getElementById('edit_link_'+link_id).value;
    var description = document.getElementById('edit_description_'+link_id).value;
    var post_request = "sid="+session_id+"&act=update&id="+link_id+"&link="+link+"&description="+description;
    if (search != '') {
        post_request += "&search="+search
    }
    var response_id = '#link_'+link_id;
    sendRequest(post_request, response_id);
}

function cancelUpdateLink(link_id, session_id, search) {
    var post_request = "sid="+session_id+"&act=cancel&id="+link_id;
    if (search != '') {
        post_request += "&search="+search
    }
    var response_id = '#link_'+link_id;
    sendRequest(post_request, response_id);
}

function deleteLink(link_id, link, session_id) {
    if (confirm('Удалить ссылку "'+link+'"?')) {
        var post_request = "sid=" + session_id + "&act=delete&id=" + link_id;
        var response_id = '#link_' + link_id;
        sendRequest(post_request, response_id);
    }
}