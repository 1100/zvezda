window.onload = function () {
    document.getElementById("new_passwd").onchange = validatePassword;
    document.getElementById("new_passwd_repeat").onchange = validatePassword;
};

function validatePassword(){
    var pass2=document.getElementById("new_passwd_repeat").value;
    var pass1=document.getElementById("new_passwd").value;
    if(pass1!=pass2) {
        document.getElementById("err_mes").innerHTML = "Пароли не совпадают!";
        return false;
    }
    else {
        document.getElementById("err_mes").innerHTML = "";
        return true;
    }
}